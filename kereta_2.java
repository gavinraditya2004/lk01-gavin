import java.util.ArrayList;
import java.util.List;

public class Kereta {
    private String nama;
    private int jumlahTiketTersedia;
    private List<Ticket> listTiket = new ArrayList<>();

    // Default constructor
    public Kereta() {
        this.jumlahTiketTersedia = 1000;
    }

    // Overload constructor
    public Kereta(String nama, int jumlahTiketTersedia) {
        this.nama = nama;
        this.jumlahTiketTersedia = jumlahTiketTersedia;
    }

    // Method tambahTiket untuk kereta komuter
    public void tambahTiket(String nama) {
        if (jumlahTiketTersedia > 0) {
            listTiket.add(new Ticket(nama));
            jumlahTiketTersedia--;
            System.out.println("==============================");
            System.out.println("Tiket berhasil dipesan");
            if (jumlahTiketTersedia < 30) {
                System.out.println("Sisa tiket tersedia: " + jumlahTiketTersedia);
            }
        } else {
            System.out.println("==============================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

    // Method tambahTiket untuk KAJJ
    public void tambahTiket(String nama, String asal, String tujuan) {
        if (this.nama.equals("Jayabaya")) {
            if (this.jumlahTiketTersedia > 0) {
                Ticket tiket = new Ticket(nama, asal, tujuan);
                this.listTiket.add(tiket);
                this.jumlahTiketTersedia--;
                System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: " + this.jumlahTiketTersedia);
            } else {
                System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
            }
        } else {
            System.out.println("Tipe kereta tidak dikenali");
        }
    }
    
    // Method untuk menampilkan daftar tiket pada kereta
    public void tampilkanTiket() {
        System.out.println("==============================");
        System.out.println("Daftar penumpang kereta api " + this.nama + " :");
        System.out.println("==============================");
        if (listTiket.isEmpty()) {
            System.out.println("Tidak ada tiket yang dipesan");
        } else {
            for (Ticket tiket : listTiket) {
                System.out.println(tiket.toString());
            }
        }
    }
}
